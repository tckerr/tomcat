from django.http import HttpResponse, JsonResponse
from django.shortcuts import render_to_response
from models import Keyword, Alias, Answer
from itertools import chain
from tomcat.genie import getUnknownAnswer
def ask(request):
	input_string = request.GET.get('about', '');
	keywords = Keyword.objects.all()
	aliases = Alias.objects.all()
	master_list = []
	for keyword in keywords:
		if keyword.string in input_string:
			master_list.append(keyword)
	for alias in aliases:
		if alias.string in input_string:
			master_list.append(Keyword.objects.filter(pk=alias.keyword.pk)[0])
	#obj_list = Keyword.objects.filter(string__in=keywords)
	#alias_list = Alias.objects.filter(string__in=keywords)
	#related_list = Keyword.objects.filter(pk__in=[alias.keyword.pk for alias in alias_list])
	#full_list = list(chain(obj_list,related_list))
	# full_list now contains the keywords
	answers = Answer.objects.filter(keywords__in=[item.pk for item in master_list]).order_by('-priority')
	if answers and answers[0]:
		answer = {
			"question":answers[0].question,
			"answer":answers[0].response,
			"type": str(answers[0].answertype)
		}
	else:
		query = input_string
		answer = getUnknownAnswer(query)

	return JsonResponse(answer)