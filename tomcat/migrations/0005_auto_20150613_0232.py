# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tomcat', '0004_keyword_dump'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnswerType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=64)),
            ],
        ),
        migrations.AddField(
            model_name='answer',
            name='answertype',
            field=models.ForeignKey(default=1, to='tomcat.AnswerType'),
        ),
    ]
