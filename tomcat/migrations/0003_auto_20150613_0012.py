# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tomcat', '0002_answer_question'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='priority',
            field=models.IntegerField(default=1),
        ),
    ]
