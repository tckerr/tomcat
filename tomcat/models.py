from django.db import models
from itertools import chain

class Keyword(models.Model):
	string = models.CharField(max_length=30)
	dump = models.BooleanField(default=True)

	def __unicode__(self):              # __unicode__ on Python 2
		return str(self.string)

class Alias(models.Model):
	string = models.CharField(max_length=30)
	keyword = models.ForeignKey('Keyword')

	def __unicode__(self):              # __unicode__ on Python 2
		return str(self.string)

class AnswerType(models.Model):
	description = models.CharField(max_length=64)

	def __unicode__(self):              # __unicode__ on Python 2
		return str(self.description)

class Answer(models.Model):
    response = models.CharField(max_length=800)
    question = models.CharField(max_length=128, default='')
    keywords = models.ManyToManyField("Keyword")
    priority = models.IntegerField(default=1)
    answertype = models.ForeignKey('AnswerType', default=1)

    def __unicode__(self):              # __unicode__ on Python 2
		return str(self.response)

def getAllKeywordsAndAliasStrings():
	obj_list = Keyword.objects.filter(dump=True)
	alias_list = Alias.objects.filter(keyword__dump=True)
	full_list = list(chain(obj_list,alias_list))
	return [str(x.string) for x in full_list]
