import urllib

def getUnknownAnswer(query=None):
	question = query if query else ''
	answer = {
			"question": urllib.quote(question),
			"answer": "Sorry, I don't understand.",
			"type": "Unknown"
	}
	return answer