from django.contrib import admin
from .models import Keyword, Alias, Answer, AnswerType

class AnswerInline(admin.TabularInline):
    model = Answer.keywords.through

class AliasInline(admin.TabularInline):
    model = Alias
    extra = 3

class KeywordAdmin(admin.ModelAdmin):
	inlines = [AnswerInline, AliasInline]

admin.site.register(Keyword, KeywordAdmin)
admin.site.register(Alias)
admin.site.register(Answer)
admin.site.register(AnswerType)