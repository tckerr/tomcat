from django.conf.urls import include, url
from django.contrib import admin
from tomcat import views

urlpatterns = [
    
    #default URL
    url(r'^$', 'tckerr.views.home', name='home'),
    url(r'^ask/$', 'tomcat.views.ask', name='ask'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
]
