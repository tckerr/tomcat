from django.http import HttpResponse, JsonResponse
from django.shortcuts import render_to_response
from tomcat.models import getAllKeywordsAndAliasStrings

def home(request):
	
	keywords = getAllKeywordsAndAliasStrings()
	
	return render_to_response('home.html', {'keywords': keywords})